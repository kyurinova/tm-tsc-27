package ru.tsc.kyurinova.tm.exception.empty;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error. Status is empty.");
    }

}

package ru.tsc.kyurinova.tm;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.component.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}

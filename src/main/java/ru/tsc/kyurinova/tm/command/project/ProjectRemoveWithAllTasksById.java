package ru.tsc.kyurinova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractProjectCommand;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectRemoveWithAllTasksById extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-with-tasks-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project with tasks by id...";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        serviceLocator.getProjectTaskService().removeById(userId, projectId);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}

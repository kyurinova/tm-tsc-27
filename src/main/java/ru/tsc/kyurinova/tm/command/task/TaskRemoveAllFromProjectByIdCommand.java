package ru.tsc.kyurinova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskRemoveAllFromProjectByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "tasks-remove-from-project-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove tasks from project by id...";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        serviceLocator.getProjectTaskService().removeAllTaskByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}

package ru.tsc.kyurinova.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.model.User;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Domain implements Serializable {

    @NotNull
    private List<User> users;

    @NotNull
    private List<Project> projects;

    @NotNull
    private List<Task> tasks;

}
